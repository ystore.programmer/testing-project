<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wplongevityblog' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lC0h1t2/6A}j6Jd{>y-/~np{Xm_+%wUXQ5G35;lMy%h.F0e-/nZ2}->jb@.2$6v%' );
define( 'SECURE_AUTH_KEY',  '1au7%G^8NLECUhZ_?N?UR vg}`K#k?ixm{nglDMVYA#. Ss@6_5nE+xZY8ge1(1l' );
define( 'LOGGED_IN_KEY',    '(C}J@L|<^_&GK=`fGFB)m<~jT$P3 =>7Qtn&~d-2Lr7^=+7IjLhhf*_i$:u!i&eu' );
define( 'NONCE_KEY',        'xB|``.<oTe@+P9?qZN3`RLvRU*-pz5]-q,:t6G6|Y?KkmYM#A3nPOTM]ECX79)(&' );
define( 'AUTH_SALT',        'V3Z;%.zKhd-Il2v$1X>*ca?A={nVOc{E.fyJZpFUano2d!<Oj3/ <!mP5l8+Uc57' );
define( 'SECURE_AUTH_SALT', 'i{g)OCqGG+PQAny>m[((3N[&|9Z~_6^[nR3IqS3kHCIMgvF8P IE]M3V``~AJ>_Y' );
define( 'LOGGED_IN_SALT',   '|XAd7iwk^SRs:{d#R/^7i1SA:#94<:&pF+[/f+mH<WDzPwDT9G>cV+N;0?)S ~Oj' );
define( 'NONCE_SALT',       '8hG4&#uMCpV0@x~h$0k{BxMBB!%`;fG*su-v*j~;s-B~-0KMB4@jl_HSh?#mFiWQ' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', 1 );
define( 'WP_DEBUG_LOG', '/tmp/wp-errors.log' );
define( 'WP_DEBUG_DISPLAY', 1 );



/* Add any custom values between this line and the "stop editing" line. */

define( 'WP_MEMORY_LIMIT', '512M' );
@ini_set( 'upload_max_size' , '512M' );
@ini_set( 'post_max_size', '512M');
@ini_set( 'memory_limit', '512M' );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
